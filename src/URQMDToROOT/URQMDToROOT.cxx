#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <vector>

#include "TString.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include "TVector3.h"
#include "TLorentzVector.h"

#include "URQMDPidToGEANTPid.h"
#include "URQMDTrack.h"
#include "URQMDEvent.h"

/*********************************************************
         WARNING WARNING WARNING WARNING WARNING

This code assumes that the time URQMD is allowed to 
propagate the event for is 200 fm/c. It is used to 
identify the line of a track in the .f13 file. See the
value of runTime below.
***********************************************************/

void URQMDToROOT(TString fileName, TString outDir, Bool_t reflect=false){

  //Some constants used to read the .f13 files
  //Only Change these if you know what you're doing!
  const int maxLineLength=308; //In characters   
  const int maxColumns=23;
  const char *delim = " ";
  const TString runTime("0.20000000E+03");

  //Get the Input File
  ifstream input;
  input.open(fileName.Data());
  if (!input.good()){
    cout <<"File not found or opened!\n";
    return;
  }

  //Determine the reflection value
  Double_t reflection(1.0);
  if (reflect)
    reflection = -1.0;

  //Define the URQMD Event
  URQMDEvent event;
  Long64_t eventCounter(0);
  
  //Create an outputFile Base name
  TString outFileName(fileName(0,fileName.Length()-4));
  TString dataFileName(outFileName);
  while (dataFileName.First("/") >= 0){
    int index = dataFileName.First("/");
    dataFileName = dataFileName(index+1,dataFileName.Length());
  }
  outDir+="/";  
  dataFileName.Prepend(outDir.Data());

  //Create an output root file to hold all the event and track information
  dataFileName.Append("_UrQMD.root");
  TFile *dataFile = new TFile(dataFileName,"RECREATE");
  TTree *dataTree = new TTree("UrQMDEvents","UrQMDEvents");
  dataTree->Branch("Event",&event,10000000);

  cout <<"Data Will be written to: " <<dataFileName.Data() <<endl;
  

  //Read the File
  while (!input.eof()){

    //Get the a line
    char line[maxLineLength];
    input.getline(line,maxLineLength);
    
    //Columns
    char * column[maxColumns] = {};
    
    //Get the First Column
    column[0] = strtok(line,delim);
    
    if (column[0] == NULL)
      continue;
    

    //NEW EVENT
    //If it the first column has impact parameter information then we have a new event
    if (strcmp(column[0],"impact_parameter_real/min/max(fm):") == 0){

      //Check if there was a previous Event
      //If there was then write it to the tree before
      //creating a new event
      if (eventCounter > 0){

	//Fill the Tree
	dataTree->Fill();

      }//End if previous Event

      //Reset the Event Object
      event.Reset();     
      
      //Set the Properties of this Event
      event.SetEventIndex(eventCounter);
      event.SetImpactParameter(atof(strtok(0,delim)));

      //Increment the Event Counter
      eventCounter++;
      
    }//End If new Event

    //TRACK LINE
    //If the column contains a track
    else if (strcmp(column[0],runTime.Data()) == 0){

      //Create a Track Object
      URQMDTrack track;

      //Parse the Track Line
      for (int iCol=1; iCol<maxColumns; iCol++){
	column[iCol] = strtok(0,delim);
      }
      
      //NOTE: the column index is n-1 compared to the UrQMD manual
      //since the manual starts counting at 1 rather than 0
      //NOTE: THIS ONLY WORKS ON STANDARD OUTPUT FILE .f13
      track.SetEndTime(atof(column[0]));
      track.SetPositionX(atof(column[1]));
      track.SetPositionY(atof(column[2]));
      track.SetPositionZ(reflection * atof(column[3]));
      track.SetEnergy(atof(column[4]));
      track.SetMomentumX(atof(column[5]));
      track.SetMomentumY(atof(column[6]));
      track.SetMomentumZ(reflection * atof(column[7]));
      track.SetMass(atof(column[8]));
      track.SetURQMDPid(atoi(column[9]));
      track.SetIso3(atoi(column[10]));
      track.SetCharge(atoi(column[11]));
      track.SetParentCollisionNumber(atoi(column[12]));
      track.SetNumberOfCollisions(atoi(column[13]));
      track.SetParentProcess(atoi(column[14]));
      track.SetFreezeOutTime(atof(column[15]));
      track.SetFreezeOutPositionX(atof(column[16]));
      track.SetFreezeOutPositionY(atof(column[17]));
      track.SetFreezeOutPositionZ(reflection * atof(column[18]));
      track.SetFreezeOutEnergy(atof(column[19]));
      track.SetFreezeOutMomentumX(atof(column[20]));
      track.SetFreezeOutMomentumY(atof(column[21]));
      track.SetFreezeOutMomentumZ(reflection * atof(column[22]));

      track.SetGeantPid(URQMDPidToGEANTPid(track.GetURQMDPid(),track.GetCharge()));
      track.SetStartVertex(1); //Always start at this vertex
      track.SetStopVertex(0);  //Ends outside of the event

      event.AddTrack(track);

    }//End Track Line
    
  }//Read Next Line

  //Fill the Last Event
  dataTree->Fill();
  
  //Write the Tree to the file
  dataFile->cd();
  dataTree->Write();

  cout <<"Events Processed: " <<eventCounter <<endl;
  
}
