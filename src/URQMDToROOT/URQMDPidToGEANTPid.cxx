//Converts the urqmdID of a particle to the Geant3 particleID 
//used in STARSIM. 

//STARSIM pids can be found here:
//http://www.star.bnl.gov/public/comp/simu/gstar/Manual/particle_id.html

//Many thanks to Daniel Brandenburg's project here:
//https://github.com/jdbrice/pyUrQMD

#include <TRandom3.h>

#include "URQMDPidToGEANTPid.h"

int URQMDPidToGEANTPid(int urqmdID, int charge){

  TRandom3 random(0);
  double rand = random.Uniform(0,1);

  //Gamma
  if (abs(urqmdID) == 100)
    return 1;

  //***************
  //MESONS
  //***************

  //Pions
  if (urqmdID == 101){
    if (charge == 0)
      return 7;
    if (charge == 1)
      return 8;
    if (charge == -1)
      return 9;
  }
	
  //Eta
  if (urqmdID == 102)
    return 17;

  //Kaons
  if (abs(urqmdID) == 106 && charge == 0){
      if (rand < .5)
	return 16;
      else
	return 10;
  }
  if (urqmdID == 106 && charge == 1)
    return 11;
  if (urqmdID == -106 && charge == -1)
    return 12;
  
  //***************
  //BARYONS
  //*************** 

  //Nucleons
  if (urqmdID == 1 && charge == 1)
    return 14;
  if (urqmdID == -1 && charge == -1)
    return 15;
  if (urqmdID == 1 && charge == 0)
    return 13;
  if (urqmdID == -1 && charge == 0)
    return 25;

  //Sigma
  if (urqmdID == 40 && charge == 1)
    return 19;
  if (urqmdID == 40 && charge == -1)
    return 21;
  if (urqmdID == -40 && charge == 1)
    return 29;
  if (urqmdID == -40 && charge == -1)
    return 27;
  if (urqmdID == 40 && charge == 0)
    return 20;
  if (urqmdID == -40 && charge == 0)
    return 28;

  //Xi
  if (urqmdID == 49 && charge == 0)
    return 22;
  if (urqmdID == -49 && charge == 0)
    return 30;
  if (urqmdID == 49 && charge == -1)
    return 23;
  if (urqmdID == -49 && charge == 1)
    return 31;
	
  //Lambda
  if (urqmdID == 27)
    return 18;
  if (urqmdID == -27)
    return 26;

  //Omega
  if (urqmdID == 55 && charge == -1)
    return 24;
  if (urqmdID == -55 && charge == 1)
    return 32;

  //***************
  //MISC
  //***************
  //Pass through pid if it doesn't conflict with geant id
  if (urqmdID > 50)
    return urqmdID;
  else
    return -1;


}
